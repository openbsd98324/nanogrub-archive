# nanogrub


It is a grub for the memstick, running on 22 MB. 

# Content

It contains on mbr grub running on a partition a9 sd0a. 
The NetBSD netbsd-install.gz is preinstalled.
NetBSD will run into the Memory /RAM. A NetBSD installer will be started if needed. 



# Stable Current NanoGrub

Stable, Current Legacy:     wget  -c   "https://gitlab.com/openbsd98324/nanogrub/-/raw/main/v3/image-nanogrub-netbsd-linux-grub-sd0a-v3.img.gz"  



# Installation 
zcat image-XXX.img > /dev/sdXXX



# Release Info

v1. up to v3, are the images using legacy boot/mbr.

v4. EFI boot (w95 fat32) with the boot loader mbr and efi (60 mb, gzip).

v6... EFI is added to boot automatically on some notebooks. 
      (note: it requires a pendrive that allows it, if possible.)


