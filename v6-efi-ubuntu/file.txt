   wget -c --no-check-certificate   "ftp://ftp.ubuntu.com/releases/groovy/ubuntu-20.10-desktop-amd64.iso" 
   

   
Disk ubuntu-20.10-desktop-amd64.iso: 3.4 MiB, 3504640 bytes, 6845 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x00000000

Device                          Boot Start     End Sectors  Size Id Type
ubuntu-20.10-desktop-amd64.iso1          1 5746099 5746099  2.8G ee GPT
ubuntu-20.10-desktop-amd64.iso2 *        0       0       1  512B  0 Empty

Partition table entries are not in disk order.




1.) Step 1. (ORIGIN)
Device       Start     End Sectors  Size Type
/dev/sda1       64 5735483 5735420  2.8G Microsoft basic data
/dev/sda2  5735484 5745435    9952  4.9M EFI System
/dev/sda3  5745436 5746035     600  300K Microsoft basic data

1 is unchanged: Microsoft basic data   
 11 Microsoft basic data 

  1 EFI System  
 11 Microsoft basic data   



2.) Step 2. bootcamp1 (non bootable).

up to this fdisk: 


Command (m for help): p
Disk /dev/sda: 7.6 GiB, 8095006720 bytes, 15810560 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: 7EE1FFAC-4072-46B8-885E-A7EA3F9C70CF

Device      Start    End Sectors   Size Type
/dev/sda1      64 204799  204736   100M Microsoft basic data
/dev/sda2  204800 214752    9953   4.9M EFI System
/dev/sda3  215040 215640     601 300.5K Microsoft basic data

newer attempts, fdisk and use 'g'

start at 2048

mount -t msdos /dev/dk0  /media/dk0

grub-install --no-floppy --root-directory=/media/dk0 /dev/rdk0 

and change grub.cfg



  grub-install --no-floppy --root-directory=/media/dk0   /dev/rdk0
Installing for i386-pc platform.



grub-install: warning: File system `fat' doesn't support embedding.
grub-install: warning: Embedding is not possible.  GRUB can only be installed in this setup by using blocklists.  However, blocklists are UNRELIABLE and their use is discouraged..
grub-install: error: will not proceed with blocklists.


localhost# mount -t msdos /dev/dk0 /media/boot
mount: realpath `/media/boot': No such file or directory
localhost# mkdir /media/boot /media/efi
localhost# mount -t msdos /dev/dk0 /media/boot
localhost# mount  /dev/dk1 /media/efi
localhost# ls /media/efi/efi/boot/
bootx64.efi grubx64.efi mmx64.efi
localhost# grub-install --target=bootx64.efi --removable --root-directory=/media/boot --efi-directory=/media/efi 
grub-install: error: /usr/pkg/lib/grub/bootx64.efi/modinfo.sh doesn't exist. Please specify --target or --directory.


# attempt #4

I tried with : 

 grub-install  --removable --root-directory=/media/boot --efi-directory=/media/efi  /dev/rsd0 



but the pendrive is not bootable yet.

Disk /dev/sda: 7.6 GiB, 8095006720 bytes, 15810560 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: A34D23CE-7841-4BFE-B53A-DE328B3774E5

Device      Start    End Sectors   Size Type
/dev/sda1    2048 206847  204800   100M Microsoft basic data
/dev/sda2  206848 216800    9953   4.9M EFI System
/dev/sda3  217088 217688     601 300.5K Microsoft basic data
   

file v2 in this archive/directory.

https://gitlab.com/openbsd98324/nanogrub/-/blob/main/v6-efi-ubuntu/image-bootcamp2-nonbootable.img



